*** Settings ***
Documentation  Example test
Library  SeleniumLibrary

*** Variables ***
${LOGIN URL}      https://int-cockpit.ts.digital-iot.com
${BROWSER}        firefox

*** Test Cases ***
[Documentation]:Simple documentation
	Log  "Hello World"
[Documentation]:Simple documentation2
	Log  hello world!
	Log  Goodbye
	Log  goodbye!

	Open Browser To Login Page
	Submit Credentials
*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Title Should Be    Cockpit
	
Input Username
    [Arguments]    ${username}
    Input Text    username_field    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password_field    ${password}

Submit Credentials
    Click Button    button button-blue