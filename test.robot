*** Settings ***
Library  Selenium2Library
Suite Setup  Open Browser  ${HOMEPAGE}  ${BROWSER}
Test Setup  Go to  ${HOMEPAGE}
Suite Teardown  Close All Browsers
Documentation Demonstrate Selenium2Library by googling.

*** Variables ***
${HOMEPAGE}  http://www.google.fr
${BROWSER}  firefox

*** Test Cases ***
Google devops and find eficode
	[Tags]  devops  eficode  smoke
	Google and check results  devops  www.ca.com/FR/DevOps

Google devops and find its web site
	[Tags]  deveo  eficode
	Google and check results  deveo  deveo.net

Google devops and find its web site 1
	[Tags]  robot
	Google and check results  robot framework  robotframework.org

*** Keywords ***
Google and check results
	[Arguments]  ${searchkey}  ${result}
	Input Text  id=lst-ib  ${searchkey}
	Click Button  name=btnK
	Wait Until Page Contains  ${result}