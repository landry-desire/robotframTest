*** Settings ***
Library  Selenium2Library
Suite Setup  Open Browser  ${HOMEPAGE}  ${BROWSER}
Test Setup  Go to  ${HOMEPAGE}
Suite Teardown  Close All Browsers

*** Variables ***
${HOMEPAGE}  https://int-cockpit.ts.digital-iot.com
${BROWSER}  firefox

*** Test Cases ***
Google devops and find eficode
	[Tags]  Connexion cockpit
	Google devops and find eficode results  FASA04811  P@ssw0rd  COCKPIT

*** Keywords ***
Google devops and find eficode results
	[Arguments]  ${username}  ${passeword}  ${result}
	Wait Until Page Contains Element  id=IDToken1
	Wait Until Element Is Visible  id=IDToken1
	Input Text  id=IDToken1  ${username}
	Input Password  id=IDToken2  ${passeword}
	Click Button  name=Login.Submit
	Wait Until Page Contains  ${result}